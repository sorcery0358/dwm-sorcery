/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;	/* border pixel of windows */
static const unsigned int gappx     = 10;	/* gaps between windows */
static const unsigned int snap      = 32;	/* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 3;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     	/* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int vertpad            = 10;	/* vertical padding of bar */
static const int sidepad            = 10;	/* vertical padding of bar */
static const int user_bh            = 20;       /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "Terminess Nerd Font:size=10:antialias=true:autohint=true",  };
static const char dmenufont[]       = "Terminess Nerd Font:size=10:antialias=true:autohint=true";
static const char hpitems[]         = "firefox,steam,spotify,discord,scrcpy,gimp,bitwarden-desktop,whatsapp-nativefier,lxappearance,blueberry,gedit,gnome-control-center,kdeconnect,libreoffice,nitrogen,virtualbox,guitarix,transmission-gtk,vlc,manjaro-settings-manager,tutanota-desktop,PPSSPPSDL,gnome-disks,gparted,stacer,edex-ui,gnome-maps,marktest,ardour6,pulseaudio-equalizer-gtk,nemo";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_aliz[]        = "#e44138";
static const char col_aliz2[]       = "#1a1a1a";
static const char col_aliz3[]       = "#2b2b2b";
static const char col_azul[]        = "#3498db";
static const char col_azul2[]       = "#14161b";
static const char col_azul3[]       = "#22252c";
static const char col_sea[]         = "#2eb398";
static const char col_sea2[]        = "#141a1b";
static const char col_sea3[]        = "#222b2e";
static const char *colors[][3]      = {
	     /*                        fg         bg         border   			*/
	[SchemeNorm] 	  =	{ col_gray3, col_aliz2, col_gray2 },
	[SchemeSel]  	  =	{ col_aliz, col_aliz3, col_aliz },
        [SchemeStatus]    = 	{ col_gray3, col_aliz2, "#000000" },
        [SchemeTagsSel]   = 	{ col_aliz, col_aliz, "#000000" },
        [SchemeTagsNorm]  = 	{ col_gray3, col_aliz2, "#000000" },
        [SchemeInfoSel]   = 	{ col_aliz, col_aliz2, "#000000" },
        [SchemeInfoNorm]  = 	{ col_gray3, col_aliz2, "#000000" },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const unsigned int ulinepad	= 0;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 2;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class		instance    title       tags mask     isfloating   monitor */
        { "QjackCtl", 		NULL,       NULL,       0,            1,           -1 },
	{ "Guitarix",		NULL,	    NULL,	0,	      1,	   -1 },
	{ "kdeconnect",		NULL,	    NULL,	0,	      1,	   -1 },
	{ "GParted",		NULL,	    NULL,	0,	      1,	   -1 },
	{ "Nitrogen",		NULL,	    NULL,	0,	      1,	   -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "│ 󰣇  =",    tile },    	/* first entry is default */
//	{ "│    =",    tile },         /* first entry is default */
	{ "│ ><> ",      NULL },    	/* no layout function means floating behavior */
	{ "│ [M] ",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] 		= { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_aliz2, "-nf", col_gray3, "-sb", col_aliz, "-sf", col_aliz2, "-hp", hpitems, "-h", "20", "-x", "10", "-y", "10", "-z", "1900", "-p", "󰣇 ", NULL };
static const char *termcmd[]  		= { "st", NULL };
static const char *screenshot[] 	= { "xfce4-screenshooter", NULL };
static const char *calculator[] 	= { "gnome-calculator", NULL };
static const char *dual_screen[] 	= { "dual-screen.sh", NULL };
static const char *whatsapp[]   	= { "whatsapp-nativefier", NULL };
static const char *killallnet[] 	= { "nmcli", "radio", "all", "off", NULL };
static const char *keylayus[] 		= { "setxkbmap", "us", NULL };
static const char *keylaytr[] 		= { "setxkbmap", "tr", NULL };
static const char *xkillcmd[]   	= { "xkill", NULL };
static const char *upvol[]      	= { "pamixer", "--allow-boost", "--increase", "5",     NULL };
static const char *downvol[]   		= { "pamixer", "--allow-boost", "--decrease", "5",     NULL };
static const char *mutevol[] 	   	= { "mutevol.sh", NULL };
static const char *mutemic[]  	 	= { "pamixer", "--default-source", "--toggle-mute", NULL };
static const char *light_up[]   	= {"light", "-A", "5", NULL};
static const char *light_down[] 	= {"light", "-U", "5", NULL};
static const char *toggle_trackpoint[] 	= {"toggle-trackpoint.sh", NULL};
static const char *toggle_trackpad[]  	= {"toggle-trackpad.sh", NULL};
static const char *ussr_anthem[]    	= {"xdg-open", "https://youtu.be/gGuvWwmSH3Q?si=BaGD9uxwDCE00tCF", NULL};

#include "exitdwm.c"

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
        { MODKEY|ShiftMask,             XK_u,      spawn,          {.v = keylayus } },
        { MODKEY|ShiftMask,             XK_t,      spawn,          {.v = keylaytr } },
	{ MODKEY|ShiftMask,             XK_k,      spawn,          {.v = xkillcmd } },
	{ 0,                            XK_Print,  spawn,	   {.v = screenshot } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      exitdwm, 	   {0} },
	{ 0,                            XF86XK_AudioMute,		spawn, {.v = mutevol } },
	{ 0,                            XF86XK_AudioLowerVolume,	spawn, {.v = downvol } },
	{ 0,                            XF86XK_AudioRaiseVolume,	spawn, {.v = upvol } },
	{ 0,                            XF86XK_AudioMicMute, 		spawn, {.v = mutemic } },
	{ 0,				XF86XK_MonBrightnessDown,	spawn, {.v = light_down} },
	{ 0,			     	XF86XK_MonBrightnessUp,		spawn, {.v = light_up} },
	{ 0,                            XF86XK_Display,			spawn, {.v = dual_screen } },
	{ 0,                            XF86XK_WLAN,               	spawn, {.v = killallnet } },
	{ 0,                            XF86XK_Messenger,               spawn, {.v = whatsapp } },
	{ 0,                            XF86XK_Calculator,              spawn, {.v = calculator } },
	{ 0,                            XF86XK_Favorites,               spawn, {.v = ussr_anthem } },
	{ MODKEY|ShiftMask,             XK_grave,      			spawn, {.v = toggle_trackpoint } },
	{ MODKEY|ShiftMask,             XK_backslash,      		spawn, {.v = toggle_trackpad } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};