#!/bin/sh

# A modular status bar for dwm
# Joe Standring <git@joestandring.com>
# GNU GPLv3

# Dependencies: xorg-xsetroot

# Import functions with "$include /route/to/module"
# It is recommended that you place functions in the subdirectory ./bar-functions and use: . "$DIR/bar-functions/example.sh"

# Store the directory the script is running from
LOC=$(readlink -f "$0")
DIR=$(dirname "$LOC")

# Change the appearance of the module identifier. if this is set to "unicode", then symbols will be used as identifiers instead of text. E.g. [📪 0] instead of [MAIL 0].
# Requires a font with adequate unicode character support
#export IDENTIFIER="unicode"

# Change the charachter(s) used to seperate modules. If two are used, they will be placed at the start and end.
export SEP0=" │"
export SEP1=" │ "

# Import the modules
. "$DIR/bar-functions/resources.sh"
. "$DIR/bar-functions/spotify.sh"
. "$DIR/bar-functions/keyboard.sh"
. "$DIR/bar-functions/backlight.sh"
. "$DIR/bar-functions/pulse.sh"
. "$DIR/bar-functions/battery.sh"
. "$DIR/bar-functions/time_date.sh"

# Update dwm status bar every second
while true
do
    # Append results of each func one by one to the upperbar string
    upperbar=""
    upperbar="$upperbar$(resources)"
    upperbar="$upperbar$(spotify)"
    upperbar="$upperbar$(keyboard)"
    upperbar="$upperbar$(backlight)"
    upperbar="$upperbar$(pulse)"
    upperbar="$upperbar$(battery)"
    upperbar="$upperbar$(time_date)"

    # Append results of each func one by one to the lowerbar string
    lowerbar=""

    xsetroot -name "$upperbar"
    # Uncomment the line below to enable the lowerbar
    #xsetroot -name "$upperbar;$lowerbar"
    sleep 0.1
done
