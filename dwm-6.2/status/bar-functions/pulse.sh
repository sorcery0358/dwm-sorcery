#!/bin/sh

# A dwm_bar function to show the master volume of PulseAudio
# Joe Standring <git@joestandring.com>
# GNU GPLv3

# Dependencies: pamixer

pulse () {
    VOL=$(pamixer --get-volume)
    STATE=$(pamixer --sink 0 --get-mute)
    MIC_STATE=$(pamixer --default-source --get-mute)

    printf "%s" "$SEP1"
        if [ "$STATE" = "true" ] || [ "$VOL" -eq 0 ]; then
            printf "󰸈 %s%%" "$VOL"
        elif [ "$VOL" -gt 0 ] && [ "$VOL" -le 33 ]; then
            printf "󰕿 %s%%" "$VOL"
        elif [ "$VOL" -gt 33 ] && [ "$VOL" -le 66 ]; then
            printf "󰖀 %s%%" "$VOL"
        else
            printf "󰕾 %s%%" "$VOL"
        fi

	if [ "$MIC_STATE" = "true" ]; then
		printf " 󰍭"
	else
		printf " 󰍬"
	fi
    printf "%s\n" "$SEP2"
}

pulse
