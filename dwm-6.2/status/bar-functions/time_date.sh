#!/bin/sh

# A dwm_bar function that shows the current date and time
# Joe Standring <git@joestandring.com>
# GNU GPLv3

# Date is formatted like like this: "[Mon 01-01-00 00:00:00]"
time_date () {
    printf "%s" "$SEP1"
    printf "󱨰  %s" "$(date "+%d/%m/%Y %A │   %H:%M")"
    printf "%s\n" "$SEP0"
}

time_date
