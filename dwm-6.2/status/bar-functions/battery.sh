#!/bin/sh

battery () {

	CHARGE=$(cat /sys/class/power_supply/BAT0/capacity)
	STATUS=$(cat /sys/class/power_supply/BAT0/status)
#	MOUSE=$(rivalcfg --battery-level | cut -c 26,27)

	printf "%s" "$SEP1"
	if [ "$STATUS" = "Discharging" ]; then
		if [ "$CHARGE" -gt 0 ] && [ "$CHARGE" -le 15 ]; then
			printf "󱃍 %s%%" "$CHARGE"
		elif [ "$CHARGE" -gt 15 ] && [ "$CHARGE" -le 50 ]; then
			printf "󱊡 %s%%" "$CHARGE"
	    	elif [ "$CHARGE" -gt 50 ] && [ "$CHARGE" -le 75 ]; then
			printf "󱊢 %s%%" "$CHARGE"
	    	else
			printf "󱊣 %s%%" "$CHARGE"
	    	fi
	else
	    	if [ "$CHARGE" -gt 0 ] && [ "$CHARGE" -le 15 ]; then
			printf "󰢟 %s%%" "$CHARGE"
            	elif [ "$CHARGE" -gt 15 ] && [ "$CHARGE" -le 50 ]; then
                	printf "󱊤 %s%%" "$CHARGE"
            	elif [ "$CHARGE" -gt 50 ] && [ "$CHARGE" -le 75 ]; then
                	printf "󱊥 %s%%" "$CHARGE"
            	else
                	printf "󱊦 %s%%" "$CHARGE"
		fi
	fi

#	if [ "$MOUSE" != "un" ]; then
#		printf " 󰍽 %s%%" "$MOUSE"
#	else
#		printf " 󰍾"
#	fi

	printf "%s\n" "$SEP2"
}

battery
