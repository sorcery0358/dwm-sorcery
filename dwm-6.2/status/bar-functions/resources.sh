#!/bin/sh

# A dwm_bar function to display information regarding system memory, CPU temperature, and storage
# Joe Standring <git@joestandring.com>
# GNU GPLv3

df_check_location='/home'

resources () {
	# get all the infos first to avoid high resources usage
	free_output=$(free --human --si | grep Mem)
	df_output=$(df -h $df_check_location | tail -n 1)
	# Used and total memory
	MEMUSED=$(echo $free_output | awk '{print substr($3,0,length($3))}')
	MEMTOT=$(echo $free_output | awk '{print substr($2,0,length($2))}')
	# CPU temperature
	CPU=$(sensors | grep CPU | awk '{print substr($2,2,length($2))}')
	# Used and total storage in /home (rounded to 1024B)
	STOUSED=$(echo $df_output | awk '{print $3-$7}')
	STOTOT=$(echo $df_output | awk '{print $2-$7}')

#	printf "%s" "$SEP1"
	printf "   %s │   %sB/%sB │   %sGB/%sGB" "$CPU" "$MEMUSED" "$MEMTOT" "$STOUSED" "$STOTOT"
	printf "%s\n" "$SEP2"
}

resources
