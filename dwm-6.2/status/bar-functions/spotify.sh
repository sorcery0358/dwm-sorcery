#!/bin/sh

# A dwm_bar function that shows the current artist, track, duration, and status from Spotify using playerctl
# Joe Standring <git@joestandring.com>
# GNU GPLv3

# Dependencies: spotify/spotifyd, playerctl

# NOTE: The official spotify client does not provide the track position or shuffle status through playerctl. This does work through spotifyd however.

spotify () {
    if ps -C spotify > /dev/null; then
        PLAYER="spotify"
    elif ps -C spotifyd > /dev/null; then
        PLAYER="spotifyd"
    fi

    if [ "$PLAYER" = "spotify" ] || [ "$PLAYER" = "spotifyd" ]; then
        ARTIST=$(playerctl --player=spotify metadata artist)
        TRACK=$(playerctl --player=spotify metadata title)
        POSITION=$(playerctl --player=spotify position --format "{{ position }}" | sed 's/.\{6\}$//')
        DURATION=$(playerctl --player=spotify metadata mpris:length | sed 's/.\{6\}$//')
        STATUS=$(playerctl --player=spotify status)
        SHUFFLE=$(playerctl --player=spotify shuffle)

        if [ "$STATUS" = "Playing" ]; then
            STATUS="󰐊"
        else
            STATUS="󰏤"
        fi

        if [ "$PLAYER" = "spotify" ]; then
            printf "%s  %s - %s " "$SEP1" "$ARTIST" "$TRACK"
            printf " %s %0d:%02d" "$STATUS" $(((DURATION-POSITION)%3600/60)) $(((DURATION-POSITION)%60))
            printf "%s\n" "$SEP2"
        fi
    fi
}

spotify
