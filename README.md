# dwm-sorcery
a custom build of suckless's dwm (dynamic window manager for X) <br>
it is suitable with the colors of matcha gtk theme; <br>
https://github.com/vinceliuice/Matcha-gtk-theme
###
## recommended to use it with these custom builds of suckless software: 
dmenu-sorcery; https://gitlab.com/sorcery0358/dmenu-sorcery <br>
st-sorcery; https://gitlab.com/sorcery0358/st-sorcery <br>
slock-sorcery; https://gitlab.com/sorcery0358/slock-sorcery
## patches
this custom build of dwm includes these patches:
###
bar height; https://dwm.suckless.org/patches/bar_height <br>
barpadding; https://dwm.suckless.org/patches/barpadding <br>
colorbar; https://dwm.suckless.org/patches/colorbar <br>
exitmenu; https://dwm.suckless.org/patches/exitmenu <br>
fancybar; https://dwm.suckless.org/patches/fancybar <br>
fullgaps; https://dwm.suckless.org/patches/fullgaps <br>
hide vacant tags; https://dwm.suckless.org/patches/hide_vacant_tags <br>
restartsig; https://dwm.suckless.org/patches/restartsig <br>
statusallmons; https://dwm.suckless.org/patches/statusallmons <br>
systray; https://dwm.suckless.org/patches/systray <br>
underlinetags; https://dwm.suckless.org/patches/underlinetags
## installation
	$ cd dwm-sorcery/dwm-6.2
	$ sudo make clean install
## dwm-bar
this custom build includes a status bar by joestandring <br>
dwm-bar; https://github.com/joestandring/dwm-bar
###
to activate it run these commands;
###
	$ cd dwm-sorcery/dwm-6.2/status
	$ chmod +x dwm-bar.sh
###
and add this into your `.xinitrc`
###
	dwm-sorcery/dwm-6.2/status/dwm-bar.sh &
## customization
to customize edit `dwm-sorcery/dwm-6.2/config.def.h` and run these commands;
###
	$ sudo cp config.def.h config.h
	$ sudo make clean install
###
and restart dwm
##
`dwm-sorcery/dwm-6.2.tar.gz` is the vanilla version of dwm